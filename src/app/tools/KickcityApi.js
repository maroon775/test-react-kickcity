import axios from 'axios';

const API_PATH = 'https://kickcity.io/api';

class KickcityApi {
	constructor(params)
	{
		this.api = axios.create({baseURL:API_PATH});
	}
	
	get(path, params)
	{
		return this.api.get(path, params);
	}
	
	getLocations()
	{
		return {
			1:{
				id   :1,
				title:'Houston',
				code :'Houston',
			},
			2:{
				id   :2,
				title:'Moscow',
				code :'Moscow',
			},
			3:{
				id   :3,
				title:'Saint-Petersburg',
				code :'SaintPetersburg',
			},
			4:{
				id   :4,
				title:'Helsinki',
				code :'Helsinki',
			},
			5:{
				id   :5,
				title:'Toronto',
				code :'Toronto',
			},
			6:{
				id   :6,
				title:'Dubai',
				code :'Dubai',
			},
		}
	}
	
}

KickcityApi.getImageUrl = (id) =>{
	return 'https://kickcity.io/pics/' + id;
}

export default KickcityApi;
