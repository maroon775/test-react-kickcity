import React, {Component} from 'react';
import {connect} from "react-redux";

import {HeaderMenu,SearchBox,Events} from './components';

class App extends Component {
	
	render()
	{
		let location = this.props.locations.current && (<b>{this.props.locations.current['title']}</b>);
		let category = this.props.categories.current && (<b>{this.props.categories.current['title']}</b>);
		return (
			<div className="page">
				<header className="page__header">
					<nav className="header-nav">
						<div className="container">
							<div className="header-nav__left">
								<h1 className="logotype logotype_header">
									<a href="/" className="logotype__link">KICKCITY</a>
								</h1>
								<HeaderMenu/>
							</div>
							<div className="header-nav__right">
								<SearchBox/>
								
								<div className="header-notify">
									<a href="#" className="header-notify__toggle header-notify__toggle_unread"><i className="icon icon_alarm-bell"></i></a>
								</div>
								<div className="header-nav__item header-nav__item_create-event header-nav__item_bd-l">
									<a href="#" className="header-nav__link">Create Event</a>
								</div>
								<div className="header-nav__item header-nav__item_userpic header-nav__item_bd-l">
									<div className="user">
										<div className="user__balance"><span className="user__balance-amount">1234</span> KYC</div>
										<a href="#" className="user__avatar">
											<img src="https://randomuser.me/api/portraits/men/79.jpg" alt=""/>
										</a>
									</div>
								</div>
							</div>
						</div>
					</nav>
					<section className="header-banner">
						<div className="container">
							<div className="header-banner__content">
								<h2 className="header-banner__title">Get rewarded <br/>for enjoing life</h2>
								<p className="header-banner__text">Earn crypto tokens by having fun with friends</p>
								<a href="#" className="header-banner__button">Get Started</a>
							</div>
						</div>
					</section>
				</header>
				<main className="page__main">
					<Events />
				</main>
			</div>
		);
	}
}

export default connect(state => ({
	locations :state.locationsReducer,
	categories:state.categoriesReducer,
}), dispatch => ({}))(App);
