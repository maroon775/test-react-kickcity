import React, {Component} from 'react';
import classNames from 'classnames';
import {connect} from "react-redux";
import eventsLoadAction from "../actions/events/eventsLoadAction";

class SearchBox extends Component {
	constructor(props)
	{
		super(props);
		
		this.state = {
			expanded:false
		};
		
		this.obSearchInput = null;
		this.timerCollapse = null;
		
		this.onBlurInput = this.onBlurInput.bind(this);
		this.onFocusInput = this.onFocusInput.bind(this);
		this.collapseBox = this.collapseBox.bind(this);
		this.onSubmitForm = this.onSubmitForm.bind(this);
		this.expandBox = this.expandBox.bind(this);
	}
	
	render()
	{
		let expandedClass = this.state.expanded ? 'search-box_expanded' : '';
		
		let classes = classNames('search-box', 'search-box_header', expandedClass);
		
		return (<form className={classes} action="/" method="GET" onSubmit={this.onSubmitForm}>
			<div className="search-box__inner">
				<input ref={(item) => this.obSearchInput = item}
					onBlur={this.onBlurInput}
					onFocus={this.onFocusInput}
					className="search-box__input"
					type="search"
					name="q" placeholder="Example: cats exhibition"/>
			</div>
			<button type="submit" className="search-box__button search-box__button_submit">Search</button>
			<button type="button" className="search-box__button search-box__button_reset" onClick={this.collapseBox}><i className="icon icon_close-blue"></i></button>
			<button type="button" className="search-box__button search-box__button_find" onClick={this.expandBox}><i className="icon icon_magnifier"></i></button>
		</form>);
	}
	
	onSubmitForm(event)
	{
		event.preventDefault();
		this.props.searchEvents(this.obSearchInput.value);
		return false;
	}
	
	onBlurInput()
	{
		if(this.obSearchInput.value.length <= 0)
		{
			this.timerCollapse = setTimeout(this.collapseBox, 3000);
		}
	}
	
	onFocusInput()
	{
		if(this.timerCollapse)
		{
			clearTimeout(this.timerCollapse);
		}
	}
	
	collapseBox()
	{
		this.obSearchInput.value = '';
		this.props.searchEvents('');
		this.setState({
			expanded:false
		});
	}
	
	expandBox()
	{
		this.setState({
			expanded:true
		});
	}
	
	componentDidUpdate()
	{
		if(this.state.expanded === true)
		{
			this.obSearchInput.focus();
		}
	}
}

export default connect(
	state => ({}),
	dispatch => ({
		searchEvents:(query) =>
			dispatch(eventsLoadAction(query))
	})
)(SearchBox);
