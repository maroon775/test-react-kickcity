import React, {Component} from 'react';
import {connect} from "react-redux";
import categoryChangeAction from "../actions/categories/categoryChangeAction";
import eventsLoadAction from "../actions/events/eventsLoadAction";
import KickcityApi from "../tools/KickcityApi";

class Events extends Component {
	constructor(props)
	{
		super(props);
		
		this.categorySelect = this.categorySelect.bind(this);
		
		if(this.props.events.length <= 0)
		{
			this.props.loadEvents();
		}
	}
	
	render()
	{
		let location = this.props.locations.current && (<b>{this.props.locations.current['title']}</b>);
		let category = this.props.categories.current && (<b>{this.props.categories.current['title']}</b>);
		let events = null;
		let dateFormat = function(dateStr){
			let date = new Date(dateStr.toString()
				.replace(/-/g, '/')
				.replace(/[T|Z]/g, ' ')
				.replace(/\+[\d]{2}\:[\d]{2}/g, '')
				.replace(/\.[\d]+$/, ''));
			let monthNames = [
				"January", "February", "March",
				"April", "May", "June",
				"July", "August", "September",
				"October", "November", "December"
			];
			let shortMonthName = monthNames[date.getMonth()].substr(0, 3);
			return shortMonthName + ', ' + date.getDate();
		};
		
		if(this.props.events.length > 0)
		{
			
			events = this.props.events.map((item, index) => (
				<article key={index} className="event-item">
					<div className="event-item__inner">
						<div className="event-item__cover">
							<img src={KickcityApi.getImageUrl(item.cover)} alt={item.title} className="event-item__cover-image"/>
						</div>
						<div className="event-item__header">
							<div className="event-item__labels">
								{item.hasTickets === "true" && (<span className="event-label">
								<i className="icon icon_label-coin"></i>
								<span className="event-label__tooltip">
									Visit this event end earn KCY tokens
								</span>
							</span>)}
								{item.hasFreeTickets === "true" && (
									<span className="event-label event-label_free">Free</span>)}
							</div>
							<time dateTime={item.startDateTime} className="event-item__date">{dateFormat(item.startDateTime)}</time>
						</div>
						<div className="event-item__footer">
							<div className="event-item__actions">
								<a href="#" className="event-item__favorite"></a>
							</div>
							<div className="event-item__title">{item.title}</div>
							{item.categoryId && (<ul className="event-item__meta">
								<li className="event-item__meta-item" data-category={item.categoryId} onClick={this.categorySelect}>{this.props.categories.items[item.categoryId].title}</li>
							</ul>)}
						</div>
						<a href={"https://kickcity.io/event/" + item.id} target="_blank" className="event-item__anchor"></a>
					</div>
				</article>
			));
		}
		let content = (
			<div className="container trending-events">
				<div className="trending-events__header">
					{events && (<a className="trending-events__more" href="#moreEvents">More <span>&rarr;</span></a>)}
					<h2 className="trending-events__title">
						Trending events
						{category && (<span> / {category}</span>)}
						<i className="icon icon_hot-fire"></i>
					</h2>
				</div>
				{events ? <div className="trending-events__items">{events}</div> : <p style={{"marginTop":"25px"}}>No events for your criteries</p>}
			</div>);
		
		return content;
	}
	
	categorySelect(event)
	{
		let categorySelected = event.target.dataset['category'];
		
		if(this.props.categories.current && categorySelected === this.props.categories.current.id)
		{
			return false;
		}
		
		if(categorySelected && categorySelected in this.props.categories.items)
		{
			let category = this.props.categories.items[categorySelected];
			this.props.changeCategory(category);
		}
	}
	
	componentDidMount()
	{
		console.log('EVENTS didMount', this.props);
	}
}

export default connect(
	state => ({
		events    :state.eventsReducer,
		categories:state.categoriesReducer,
		locations :state.locationsReducer
	}),
	dispatch => ({
		loadEvents    :() =>
			dispatch(eventsLoadAction()),
		changeCategory:(category) =>
			dispatch(categoryChangeAction(category)),
	})
)(Events);
