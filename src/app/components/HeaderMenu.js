import React, {Component} from 'react';
import {connect} from "react-redux";
import categoriesLoadAction from '../actions/categories/categoriesLoadAction';
import categoryChangeAction from '../actions/categories/categoryChangeAction';
import locationsLoadAction from '../actions/locations/locationsLoadAction';
import locationChangeAction from '../actions/locations/locationChangeAction';

class HeaderMenu extends Component {
	constructor(props)
	{
		super(props);
		
		if(Object.keys(this.props.categories.items).length <= 0)
		{
			this.props.loadCategories();
		}
		
		if(Object.keys(this.props.locations.items).length <= 0)
		{
			this.props.loadLocations();
		}
		else
		{
			if(!this.props.locations.current)
			{
				this.props.changeLocation(this.props.locations.items[Object.keys(this.props.locations.items)[0]]);
			}
		}
		
		this.onCategorySelect = this.onCategorySelect.bind(this);
		this.onLocationSelect = this.onLocationSelect.bind(this);
		this.categoryClear = this.categoryClear.bind(this);
	}
	
	render()
	{
		let locations = '', locationsKeys = Object.keys(this.props.locations.items);
		if(locationsKeys.length > 0)
		{
			
			let selectedLocation = '';
			if(this.props.locations.current)
			{
				selectedLocation = this.props.locations.current;
			}
			
			locations = locationsKeys.map((index) =>{
				let item = this.props.locations.items[index];
				if(this.props.locations.current && item.id === this.props.locations.current.id)
				{
					return null;
				}
				return (<li className="nav-menu__item" key={item.id}>
					<span className="nav-menu__item-value" onClick={this.onLocationSelect} data-location={index}>{item.title}</span>
				</li>)
			});
			
			locations = (<li className="nav-menu__item nav-menu__item_root nav-menu__item_location">
				<span className="nav-menu__item-value"><i className="icon icon_location"></i>{selectedLocation.title}</span>
				<ul className="nav-menu nav-menu_submenu">{locations}</ul>
			</li>);
		}
		
		let categories = '', categoriesKeys = Object.keys(this.props.categories.items);
		if(categoriesKeys.length > 0)
		{
			categories = categoriesKeys.map((index) =>{
				let item = this.props.categories.items[index];
				return (<li className="nav-menu__item" key={index}>
						<span className="nav-menu__item-value" onClick={this.onCategorySelect} data-category={index}>{item.title}</span>
					</li>
				);
			});
			
			categories = (<li className="nav-menu__item nav-menu__item_root nav-menu__item_categories">
				<span className="nav-menu__item-value">Categories</span>
				<ul className="nav-menu nav-menu_submenu">
					<li className="nav-menu__item">
						<span className="nav-menu__item-value" onClick={this.categoryClear}>All</span>
					</li>
					{categories}
				</ul>
			</li>);
		}
		
		return (
			<ul className="nav-menu nav-menu_header">
				{locations}
				{categories}
				<li className="nav-menu__item">
					<a href="#" className="nav-menu__item-value">Explore</a>
				</li>
			</ul>
		)
	}
	
	categoryClear()
	{
		this.props.changeCategory(null);
	}
	
	onCategorySelect(event)
	{
		let categorySelected = event.target.dataset['category'];
		
		if(this.props.categories.current && categorySelected === this.props.categories.current.id)
		{
			return false;
		}
		
		if(categorySelected && categorySelected in this.props.categories.items)
		{
			let category = this.props.categories.items[categorySelected];
			this.props.changeCategory(category);
		}
	}
	
	onLocationSelect(event)
	{
		let locationSelected = event.target.dataset['location'];
		if(locationSelected in this.props.locations.items)
		{
			this.props.changeLocation(this.props.locations.items[locationSelected]);
		}
	}
	
	componentWillReceiveProps()
	{
		if(!this.props.locations.current && Object.keys(this.props.locations.items).length > 0)
		{
			let defaultLocation = this.props.locations.items[Object.keys(this.props.locations.items)[0]];
			this.props.changeLocation(defaultLocation);
		}
	}
}

export default connect(state => ({
	locations :state.locationsReducer,
	categories:state.categoriesReducer
}), dispatch => ({
	changeLocation:(location) =>
		dispatch(locationChangeAction(location)),
	changeCategory:(category) =>
		dispatch(categoryChangeAction(category)),
	loadCategories:() =>
		dispatch(categoriesLoadAction()),
	loadLocations :() =>{
		dispatch(locationsLoadAction())
	}
}))(HeaderMenu);
