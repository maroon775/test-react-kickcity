import _HeaderMenu from './HeaderMenu';
import _SearchBox from './SearchBox';
import _Events from './Events';

export class HeaderMenu extends _HeaderMenu {};

export class SearchBox extends _SearchBox {};

export class Events extends _Events {};
export default {
	HeaderMenu,
	SearchBox,
	Events
};
