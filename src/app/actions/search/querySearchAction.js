import eventsLoadAction from '../events/eventsLoadAction';

const querySearchAction = (query) => (dispatch) =>{
	dispatch({
		type:'TYPE_QUERY_SEARCH',
		query
	});
	
	dispatch(eventsLoadAction(query));
};

export default querySearchAction;
