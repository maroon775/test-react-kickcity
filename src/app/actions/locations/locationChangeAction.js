import eventsLoadAction from '../events/eventsLoadAction';
import store from "store";

const locationChangeAction = (location) => (dispatch) =>{
	store.set('selected-location', location);
	
	dispatch({
		type:'TYPE_LOCATION_CHANGE',
		location
	});
	
	dispatch(eventsLoadAction());
};

export default locationChangeAction;
