import KickcityApi from '../../tools/KickcityApi';
import store from 'store';

const locationsLoadAction = () => dispatch =>{
	let Api = new KickcityApi();
	let locations = store.get('cached.locations');
	
	console.log((locations && locations.data && locations.expire >= Date.now()));
	
	if(locations && locations.data && locations.expire >= Date.now())
	{
		console.log('Api get /db/locations from cache', locations);
		
		return dispatch({
			type:'TYPE_LOCATIONS_LOADED',
			data:locations.data
		});
	}
	else
	{
		let locations = Api.getLocations();
		
		console.log('Api get /db/locations', locations);
		
		store.set('cached.locations', {
			expire:Date.now() + (3600 * 1000),
			data  :locations
		});
		
		dispatch({
			type:'TYPE_LOCATIONS_LOADED',
			data:locations
		});
	}
};

export default locationsLoadAction;
