import KickcityApi from '../../tools/KickcityApi';
import store from 'store';

const categoriesLoadAction = () => dispatch =>{
	let Api = new KickcityApi();
	let categories = store.get('cached.categories');
	
	if(categories && categories.data && categories.expire >= Date.now())
	{
		console.log('Api get /db/categories from cache', categories);
		return dispatch({
			type:'TYPE_CATEGORIES_LOADED',
			items:categories.data
		});
	}
	else
	{
		Api.get('/db/categories').then(function(response){
			console.log('Api get /db/categories', response);
			let items = {};
			response.data.rows.forEach(item=>items[item.id]=item);
			
			store.set('cached.categories', {
				expire:Date.now() + (3600 * 1000),
				data  :items
			});
			
			dispatch({
				type:'TYPE_CATEGORIES_LOADED',
				items
			});
		});
	}
};

export default categoriesLoadAction;
