import eventsLoadAction from '../events/eventsLoadAction';
import store from "store";

const categoryChangeAction = (category) => (dispatch) =>{
	store.set('selected-category', category);
	
	dispatch({
		type:'TYPE_CATEGORY_CHANGE',
		category
	});
	
	dispatch(eventsLoadAction());
}

export default categoryChangeAction;
