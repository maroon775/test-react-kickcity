import KickcityApi from '../../tools/KickcityApi';
import store from 'store';

const eventsLoadAction = (query) => dispatch =>{
	let Api = new KickcityApi();
	let requestParams = {
		trend:true,
		limit:6,
		city :'Houston'
	};
	let category = store.get('selected-category');
	let location = store.get('selected-location');
	
	if(query && query.toString().length > 0)
	{
		requestParams.title = query.toString();
	}
	if(category)
	{
		requestParams.category = category.id;
	}
	if(location)
	{
		requestParams.city = location.code;
	}
	console.log(requestParams);
	
	Api.get('/search/event/feed/all', {params:requestParams}).then(function(response){
		console.log('Api get /events', response);
		
		dispatch({
			type:'TYPE_EVENTS_LOADED',
			data:response.data
		});
	});
	
};

export default eventsLoadAction;
