import store from 'store';

export default function eventsReducer(state = [], action){
	switch(action.type)
	{
		case 'TYPE_EVENTS_LOADED':
			return action.data.rows;
		default:
			return state;
	}
	
}
