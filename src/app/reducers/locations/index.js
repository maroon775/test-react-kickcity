import store from 'store';

let locations = store.get('cached.locations') || {};
let locationsInitialData = locations && 'data' in locations ? {
	current   :store.get('selected-location'),
	items     :locations.data,
	getCurrent:function(){
		return this.current;
	}
} : {
	current   :null,
	items     :{},
	getCurrent:function(){
		return this.current;
	}
};
export default function locationsReducer(state = locationsInitialData, action){
	switch(action.type)
	{
		case 'TYPE_LOCATIONS_LOADED':
			return {
				...state,
				items:action.data
			};
		case 'TYPE_LOCATION_CHANGE':
			return {
				...state,
				current:action.location
			};
		default:
			return state;
	}
	
}
