import {combineReducers} from 'redux';
import categoriesReducer from './categories';
import locationsReducer from './locations';
import eventsReducer from './events';

export default combineReducers({
	categoriesReducer,
	locationsReducer,
	eventsReducer
});
