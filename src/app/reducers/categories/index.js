import store from 'store';

var categories = store.get('cached.categories');
var categoriesInitialValues = categories && 'data' in categories ? {
	current   :store.get('selected-category'),
	items     :categories.data,
	getCurrent:function(){
		return this.current;
	}
} : {
	current   :null,
	items     :[],
	getCurrent:function(){
		return this.current;
	}
};
export default function categoriesReducer(state = categoriesInitialValues, action){
	switch(action.type)
	{
		case 'TYPE_CATEGORIES_LOADED':
			return {
				current:state.current,
				items  :action.items
			};
		case 'TYPE_CATEGORY_CHANGE':
			return {
				current:action.category,
				items  :state.items
			};
		default:
			return state;
	}
	
}
