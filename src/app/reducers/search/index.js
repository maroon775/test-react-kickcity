export default function searchReducer(state = '', action){
	switch(action.type)
	{
		case 'TYPE_QUERY_SEARCH':
			return action.query;
		default:
			return state;
	}
	
}
